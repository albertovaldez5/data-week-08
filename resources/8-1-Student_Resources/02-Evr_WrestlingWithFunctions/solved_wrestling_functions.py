import os
import csv
from pathlib import Path

# Path to collect data from the Resources folder
resources = Path("Resources")
wrestling_csv = resources / 'WWE-Data-2016.csv'
# Define the function and have it accept the 'wrestlerData' as its sole parameter
def print_percentages(row):
# Find the total number of matches wrestled
    wins = int(row[1])
    loses = int(row[2])
    draws = int(row[3])
    total = sum((wins, loses, draws))
# Find the percentage of matches won
    win_p = wins / total
# Find the percentage of matches lost
    lost_p = loses / total
# Find the percentage of matches drawn
    draws_p = draws/ total
# Print out the wrestler's name and their percentage stats
    print(f"Wrestler: {row[0]} Win %: {100*win_p:,.2f}, Lose %: {100*lost_p:,.2f}, Draw %: {100*draws_p:,.2f}")
# Read in the CSV file
with open(wrestling_csv, 'r') as csvfile:

    # Split the data on commas
    csvreader = csv.reader(csvfile, delimiter=',')

    # Prompt the user for what wrestler they would like to search for
    name_to_check = input("What wrestler do you want to look for? ")

    # Loop through the data
    for row in csvreader:

        # If the wrestler's name in a row is equal to that which the user input, run the 'print_percentages()' function
        if(name_to_check == row[0]):
            print_percentages(row)
