- [ETL process](#etl-process)
- [Python Functions](#python-functions)
- [Lambda Functions](#lambda-functions)
- [List Comprehensions](#list-comprehensions)
  - [Double Comprehension](#double-comprehension)
- [Regular Expressions](#regular-expressions)
  - [Basic Metacharacters](#basic-metacharacters)
  - [Quantifiers](#quantifiers)
  - [Sets](#sets)
  - [Special Sequences](#special-sequences)
  - [Assertion](#assertion)
  - [Repetition Metacharacters](#repetition-metacharacters)
  - [Special Groups](#special-groups)
- [Regex Cases](#regex-cases)
  - [Password Validation](#password-validation)
  - [Time Format Validation](#time-format-validation)
  - [Email Validation](#email-validation)
  - [Username Validation](#username-validation)
- [ETL Regex Cases](#etl-regex-cases)
  - [Word Extraction and Replacement](#word-extraction-and-replacement)
  - [Matching a sentence](#matching-a-sentence)
- [Regex with Functions](#regex-with-functions)
- [Generators and Globs](#generators-and-globs)



<a id="etl-process"></a>

# ETL process

Extract, Transform, Load.<sup><a id="fnr.1" class="footref" href="#fn.1" role="doc-backlink">1</a></sup> is the process of getting data into the adequate format to be used. This is what differentiates a professional process from an amateur one, as this lets you repeat the process effectively.

This is where all the data engineering process is done, transformation and loading.

In the case of Python, we need to be able to use the language&rsquo;s functions and modules to be able to transform the data effectively, which includes Regular Expressions, lambda functions and list comprehensions.


<a id="python-functions"></a>

# Python Functions

Functions wrap a procedure in a callable piece of code. They can take arguments at the start and return value at the end. They have their own scope for variables declared inside them but they can access global variables.

```python
def make_sandwich(bread, prot1, prot2="Cheese"):
    sandwich = f"Sandwich: {bread}, {prot1}, {prot2}, {bread}"
    return sandwich
sandwich = make_sandwich("Integral Bread", "Roast Beef")
print(sandwich)
```

    Sandwich: Integral Bread, Roast Beef, Cheese, Integral Bread


<a id="lambda-functions"></a>

# Lambda Functions

Lambda functions are anonymous functions that can be stored in a variable and have very simple arguments and expressions.

```python
square = lambda x: x * x

for i in range(1, 6):
    print(square(i))
```

    1
    4
    9
    16
    25

We can&rsquo;t use lambda expressions in multiline form, but we can use them as arguments to other functions. They are useful when we need a simple throwaway function that we will use once when cleaning or transforming our data.

In this case we are going to create a lambda function that receives a string, gets its length and checks if it&rsquo;s greater than 15. We can then use it with pandas&rsquo; `map` to apply it to all elements in our pandas `Series`. This will result in an index `Series` that we can use to index our dataframe.

```python
import pandas as pd

csv = "../resources/8-1-Student_Resources/04-Stu_LambdaFunctions/Resources/movies_metadata.csv"
movies_df = pd.read_csv(csv)

is_long = lambda x: len(str(x)) > 15
index = movies_df['original_title'].map(is_long)
long_titles = movies_df[index]
print(long_titles.head())
```

        adult    budget     id    imdb_id  ...                        title  video vote_average vote_count
    2   False         0  15602  tt0113228  ...             Grumpier Old Men  False          6.5       92.0
    3   False  16000000  31357  tt0114885  ...            Waiting to Exhale  False          6.1       34.0
    4   False         0  11862  tt0113041  ...  Father of the Bride Part II  False          5.7      173.0
    10  False  62000000   9087  tt0112346  ...       The American President  False          6.5      199.0
    11  False         0  12110  tt0112896  ...  Dracula: Dead and Loving It  False          5.7      210.0
    
    [5 rows x 16 columns]

We can use the lambda expression inside the map function to make it truly anonymous.

```python
short_titles = movies_df[movies_df['original_title'].map(lambda x: len(str(x))) <= 15]
print(short_titles.head())
```

       adult    budget     id    imdb_id  ...         title  video vote_average vote_count
    0  False  30000000    862  tt0114709  ...     Toy Story  False          7.7     5415.0
    1  False  65000000   8844  tt0113497  ...       Jumanji  False          6.9     2413.0
    5  False  60000000    949  tt0113277  ...          Heat  False          7.7     1886.0
    6  False  58000000  11860  tt0114319  ...       Sabrina  False          6.2      141.0
    7  False         0  45325  tt0112302  ...  Tom and Huck  False          5.4       45.0
    
    [5 rows x 16 columns]


<a id="list-comprehensions"></a>

# List Comprehensions

List comprehensions allows us to create a list using an expression in a single line of code.

```python
data = [i for i in range(20)]
print(data)
```

    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19]

We can add conditions as part of the expression. In this case we check if the number is divisible by 3.

```python
data = [i for i in range(0, 20) if i % 3 == 0]
print(data)
```

    [0, 3, 6, 9, 12, 15, 18]

We can add multiple conditions in both left and right sides of the comprehension. In this case we are adding `"FizzBuzz"` if the number is divisible by both `3` and `5`, `Fizz` if it&rsquo;s only divisible by `3` and `Buzz` if it&rsquo;s only divisible by `5`.

```python
comp_fizzbuzz = [
    "Fizz" if not i % 5 == 0 else "Buzz" if not i % 3 == 0 else "FizzBuzz"
    for i in range(0, 20) if i % 3 == 0 or i % 5 == 0
]
print(comp_fizzbuzz)
```

    ['FizzBuzz', 'Fizz', 'Buzz', 'Fizz', 'Fizz', 'Buzz', 'Fizz', 'FizzBuzz', 'Fizz']

We can rewrite the previous comprehension as regular code so we can understand it better.

```python
fizzbuzz = []
for i in range(0, 20):
    if i % 3 == 0 and i % 5 == 0:
        fizzbuzz.append("FizzBuzz")
    elif i % 3 == 0 and i % 5 != 0:
        fizzbuzz.append("Fizz")
    elif i % 5 == 0 and i % 3 != 0:
        fizzbuzz.append("Buzz")
print(fizzbuzz)
```

    ['FizzBuzz', 'Fizz', 'Buzz', 'Fizz', 'Fizz', 'Buzz', 'Fizz', 'FizzBuzz', 'Fizz']

Finally, we can use more list comprehensions to compare both lists and make sure they are the same, assuming they have the same length. We will use lambda functions for our comparison logic and then call them with `assert` as if we were doing tests.

```python
same_length = lambda l1, l2: len(l1) == len(l2)
lists_are_equal = lambda l1, l2: False not in [(a == b) for a, b in zip(l1, l2)]
# Test
assert same_length(comp_fizzbuzz, fizzbuzz)
assert lists_are_equal(comp_fizzbuzz, fizzbuzz)
print("Lists are the same!")
```

    Lists are the same!


<a id="double-comprehension"></a>

## Double Comprehension

Even though we used multiple expressions in a single comprehension, we haven&rsquo;t used nested comprehensions as in one loop inside another. Here is an example:

Let&rsquo;s say our data is the following.

```python
employees = [
    {
        "name": "Bob",
        "clients": [1, 2, 3, 4]
    },
    {
        "name": "Alice",
        "clients": [5, 6, 7, 8, 9, 10]
    }
]
```

If we wanted to access each client ID from the clients list, we can use the following loop.

```python
client_ids = []
for employee in employees:
    for client in employee["clients"]:
        client_ids.append(client)

print(client_ids)
```

    [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

If we wanted to use list comprehensions, we would do the following.

```python
comp_ids = [client for employee in employees for client in employee["clients"]]
print(comp_ids)
```

    [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

The order in the list comprehension follows the order of the nexted for loops and then we add the `client` variable at the begining of the comprehension.

Note that we could also extend the list.

```python
extend_ids = []
for employee in employees:
    extend_ids.extend(employee["clients"])

print(extend_ids)
```

    [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]


<a id="regular-expressions"></a>

# Regular Expressions

Regular expressions are formulas we can use to find patterns in our data, specifically text <sup><a id="fnr.2" class="footref" href="#fn.2" role="doc-backlink">2</a></sup>. We can use Regex in Python with the `re` module.

```python
import re
```

There are websites that can help us test Regex patterns against text.

-   <https://regexr.com/>
-   <https://regex101.com/>


<a id="basic-metacharacters"></a>

## Basic Metacharacters

-   `^` search the following characters at the start of the string.
-   `$` search the following characters at the end of the string.

```python
st = "dogs are adorable"
p = "^dogs"
p2 = "dogs$"
print(re.search(p, st))
```

    <re.Match object; span=(0, 4), match='dogs'>


<a id="quantifiers"></a>

## Quantifiers

-   `?` zero or one occurences of the preceeding element (optional instance of the element before the ? symbol).

```python
st2 = "color"
st3 = "colour"
pattern = "colou?r"
print(re.search(pattern, st2))
print(re.search(pattern, st3))
```

    <re.Match object; span=(0, 5), match='color'>
    <re.Match object; span=(0, 6), match='colour'>

-   `+` one or more occurences of the preceeding element (any repetitions of the element before the + symbol).

```python
st4 = "ac"
st5 = "abc"
st6 = "abbc"
pattern = "ab+c"
print(re.search(pattern, st4))
print(re.search(pattern, st5))
```

    None
    <re.Match object; span=(0, 3), match='abc'>


<a id="sets"></a>

## Sets

-   `-` means a range from a character to another `0-10`.
-   `.` means any single character except the new line. This is a wildcard.

A list of characters inside a pair of square brackets `[]`. `^` inside the brackets means `NOT` the following.

```python
p1 = "[^a-n]"     # Any character NOT between these.
p2 = "[0145]"     # Any of these characters.
p3 = "[0-9]"      # Any digit between these two.
p4 = "[0-5][0-9]" # Any two digit numbers from 00 to 59.
p5 = "[a-Z]"      # Any letter lower or upper case.
st = "dogs are adorable"
print(re.search("[a-d]", st))  # search first occurence.
print(re.findall("[a-d]", st)) # search all.
```

    <re.Match object; span=(0, 1), match='d'>
    ['d', 'a', 'a', 'd', 'a', 'b']


<a id="special-sequences"></a>

## Special Sequences

-   `\` followed by `d` or `w`.
-   `\d` where the string contains any digit. Same as `[0-9]`.
-   `\w` where the string contains a=\d= will match any digit from 0 to 9.

Examples:

-   `\D` will match any non-digit character.
-   `\w` matches a word character (a letter, digit, or underscore).
-   `\W` matches any non-word character (anything other than a letter, digit, or underscore, such as spaces and punctuation).
-   `\s` will match any whitespace character (including spaces, tabs, and newlines).
-   `\S` will match any non-whitespace characters.ny word chacter. Same as `[0-9a-Z_]`.

```python
st = "?my password is pass1234"
print(re.findall("\d", st))
print(re.findall("\w", st)) # not the ? symbol.
```

    ['1', '2', '3', '4']
    ['m', 'y', 'p', 'a', 's', 's', 'w', 'o', 'r', 'd', 'i', 's', 'p', 'a', 's', 's', '1', '2', '3', '4']


<a id="assertion"></a>

## Assertion

When a match is possible in some way.

1.  Lookahead assertion: Search the string after the current match position, current position of the cursor.
2.  Positve lookahead assertion: Makes sure that the pattern exists in the string. At least one occurence.

`(?=` The start of a lookahead group.

```python
r1 = "(?=.*[A-Z])"  # match if a string contains an upper case letter.
r2 = "(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9]).{6,}" # match if string contains at least a upper case letter, a lower case letter and a digit, at least 6 characters long.
str_to_validate = "hey123456"
valid = re.fullmatch(r2, str_to_validate)
print(valid)
valid = re.fullmatch(r2, "Hey123456")
print(valid)
```

    None
    <re.Match object; span=(0, 9), match='Hey123456'>


<a id="repetition-metacharacters"></a>

## Repetition Metacharacters

Match the previous expressions any number of times.

-   `{}` represents a specific number of times.
-   `{8,}` at least 8 characters.
-   `{8, 15}` within 8 to 15 characters.


<a id="special-groups"></a>

## Special Groups

-   `?:` is a non-capturing group. We only want to use the grouping structure, we don&rsquo;t want to capture the information.

```python
phone = "212-012-9876"
regex = "(?:\d{3})-(\d{3})-(?:\d{4})"
r = re.sub(regex, "555", phone)
print(r)
```

    555

-   `?!` makes sure that the following element doesn&rsquo;t exist within the match.

```python
phone = "333-333-4444"
phone2 = "333-333-55555"
regex = "(\d{3})-(\d{3})-(\d{4})(?!\d)"
print(re.match(regex, phone))
print(re.match(regex, phone2))
```

    <re.Match object; span=(0, 12), match='333-333-4444'>
    None


<a id="regex-cases"></a>

# Regex Cases


<a id="password-validation"></a>

## Password Validation

We can construct a regex to validate passwords.

```python
regex = "[A-Za-z-0-9@#$%^&+=]{8,}"
str_to_validate = "hey123456"
valid = re.fullmatch(regex, str_to_validate)
if valid:
    print("There is a match.", valid)
else:
    print("Try again.")
```

    There is a match. <re.Match object; span=(0, 9), match='hey123456'>


<a id="time-format-validation"></a>

## Time Format Validation

Determine wether the input is worth processing further with the backend. Check if formatting is correct.

```python
inputs = ["18:29", "23:55", "123", "ab:de", "18:299", "99:99"]
input1 = "12:455"
regex = "[0-9]{2}:[0-9]{2}"
print(re.fullmatch(regex, input1))
input2 = "12:48"
print(re.fullmatch(regex, input2))
```

    None
    <re.Match object; span=(0, 5), match='12:48'>

Iterating over the values.

```python
r = [re.fullmatch(regex, i) for i in inputs]
print(r)
```

    [<re.Match object; span=(0, 5), match='18:29'>, <re.Match object; span=(0, 5), match='23:55'>, None, None, None, <re.Match object; span=(0, 5), match='99:99'>]

More complex regex for a 24 hours clock.

```python
regex = "([01][0-9]|2[0-3]):([0-5][0-9])"
r = [re.fullmatch(regex, i) for i in inputs]
print(r)
```

    [<re.Match object; span=(0, 5), match='18:29'>, <re.Match object; span=(0, 5), match='23:55'>, None, None, None, None]


<a id="email-validation"></a>

## Email Validation

Construct a regex that checks for correct email format.

```python
emails = ["rog45@gmail.com", "r_duke78o@outlook.com", "s,rog78o@outlook.com"]
regex_a = "^(\w|\.|\_|\-)+" # Any alphanumerical or a period or a underscore or a hyphen.
regex_b = "[@]\w+[.]\w{2,3}$" # @, then any alphanumerical then a period then any alphanumerical length 2 or 3.
regex = f"{regex_a}{regex_b}"
r = [re.fullmatch(regex, i) for i in emails]
print(r)
```

    [<re.Match object; span=(0, 15), match='rog45@gmail.com'>, <re.Match object; span=(0, 21), match='r_duke78o@outlook.com'>, None]


<a id="username-validation"></a>

## Username Validation

Construct a regex that checks for correct username format.

```python
usernames = ["a_roger", "aroger", "a.roger_de", "a.roger_2"]
regex = "^[a-zA-Z_.]+$"
r = [re.fullmatch(regex, i) for i in usernames]
print(r)
```

    [<re.Match object; span=(0, 7), match='a_roger'>, <re.Match object; span=(0, 6), match='aroger'>, <re.Match object; span=(0, 10), match='a.roger_de'>, None]


<a id="etl-regex-cases"></a>

# ETL Regex Cases


<a id="word-extraction-and-replacement"></a>

## Word Extraction and Replacement

-   `?` is the option regex. But can also mean:

-   `*` by default returns more occurences (greedy), but we can force zero occurences, we use `?*` (non-greedy).

```python
# Greedy
text = "abcdeaskjdna"
regex = "ab.*" # a, b and then any character.
print(re.findall(regex, text))
# Non-greedy
regex = "ab.*?" # a, b and then any character but zero.
print(re.findall(regex, text))
```

    ['abcdeaskjdna']
    ['ab']

Greedy mode wants all characters until the last occurence. Non-greedy stops at the first occurence, then stops and resets its search, effectivelly returning more than one element.

```python
# Greedy
text = "peter piper picked a peck of pickled peppers"
regex = "p.*e.*r" # starts with a p and ends with an r, but there is an e in the middle.
print(re.findall(regex, text))
# Non-greedy
regex = "p.*?e.*?r"
print(re.findall(regex, text))
```

    ['peter piper picked a peck of pickled pepper']
    ['peter', 'piper', 'picked a peck of pickled pepper']


<a id="matching-a-sentence"></a>

## Matching a sentence

Return a match when the string starts with &rsquo;crypto&rsquo;, then at most 30 arbitrary characters, then ends with &rsquo;coin&rsquo;.

```python
text = "crypto-bot that is trading Bitcoin and other currencies"
regex = "crypto(.{1,30}coin)"
print(re.match(regex, text))
```

    <re.Match object; span=(0, 34), match='crypto-bot that is trading Bitcoin'>

Find all occurences of dollar amounts with optional decimal values.

```python
text = """
If you invested in $1 in the year 1801, you would have $18087791.41 today.
This is a 7.967% return on investment.
But if you invested only $0.25 in 1801, you would end up with $4521947.8525.
"""
# Escape the $ sign and the . with a backslash
regex = "(\$[0-9]+(\.[0-9]*)?)" # non-greedy
print(re.findall(regex, text))
```

    [('$1', ''), ('$18087791.41', '.41'), ('$0.25', '.25'), ('$4521947.8525', '.8525')]

Replace a name in a text. Don&rsquo;t replace occurences when the name is in quotes.

Using `?!` negative lookahead. We are going to lookeahead of the current position and if we find the specific character, we are gonna do nothing to it.

```python
text = """Alice Wonderland married John Doe.
The new name of former 'Alice Wonderland' is Alice Doe.
Alice Wonderland replaced her old name 'Wonderland' with her name 'Doe'.
Alice's sister Jane Wonderland still keeps her old name.
"""
regex = "Alice Wonderland(?!')"
replace = "Alice Doe"
print(re.sub(regex, replace, text))
```

    Alice Doe married John Doe.
    The new name of former 'Alice Wonderland' is Alice Doe.
    Alice Doe replaced her old name 'Wonderland' with her name 'Doe'.
    Alice's sister Jane Wonderland still keeps her old name.


<a id="regex-with-functions"></a>

# Regex with Functions

We can create specific functions to match different data with the same Regex.

```python
import pandas as pd
import re

pd.options.display.max_colwidth = 100
path = "../resources/8-2-Student_Resources/08-Evr_FunctionalRegex/Resources"
alice_text = f"{path}/alice.txt"
sherlock_text = f"{path}/sherlock.txt"

def count_questions(path):
    regex = r"(.*\?.*)"
    df = pd.read_csv(path, sep="\n", header=None)
    df.columns = ['text']
    values = df['text'].str.extractall(regex, flags=re.I)[0].value_counts()
    return f"Number of questions: {len(values)}"

alice = count_questions(alice_text)
sherlock = count_questions(sherlock_text)
print(alice)
print(sherlock)
```

    Number of questions: 196
    Number of questions: 705


<a id="generators-and-globs"></a>

# Generators and Globs

**NOTE**: This is an extra section about Python generators and globs which can help in the ETL process.

Generators are a function that will `yield` a value instead of using `return`, this means that it gives back control to the main procedure before going back into it&rsquo;s own loop.

```python
def get_str_length(data):
    for d in data:
        yield d, len(d)

data = ["Bob", "Alice", "Alex", "Michael Scott"]
for i in get_str_length(data):
    print(i)
```

    ('Bob', 3)
    ('Alice', 5)
    ('Alex', 4)
    ('Michael Scott', 13)

This means that we can use them in list comprehensions and combine them with other processes in our ETL pipeline.

In this case, we can open all `json` files in a given directory and stop reading files whenever we find one that matches our requirements, without needing to modify the `get_all_json` function.

This is possible in part because of the `glob` function from `Path` which uses globing <sup><a id="fnr.3" class="footref" href="#fn.3" role="doc-backlink">3</a></sup> (which is not Regex) to iterate over all files in a directory.

```python
from pathlib import Path
import pandas as pd
import json

def get_all_json(directory):
    """Generate json files from directory"""
    path = Path(directory).resolve()
    for filepath in path.glob("*.[json]*"):
        with open(filepath, 'rb') as file:
            yield json.load(file)

for data in get_all_json("../resources/jsons"):
    print("JSON file:", data['name'])
    df = pd.DataFrame(data)
    if df['data'].size < 8:
        break

print(df.head()['data'])
```

    JSON file: theme
    JSON file: org-template
    JSON file: installers
    0    {'id': 'inlcuding-files-for-the-installer-to-f...
    1    {'id': 'src-inlcuding-files-for--1', 'text': '...
    2    {'id': 'src-inlcuding-files-for--2', 'text': '...
    Name: data, dtype: object

We don&rsquo;t care about all other possible dataframes in our dataset, we only want the first one that matches our requirement so we don&rsquo;t need a list to hold them all.

We could make a list comprehension to include all other dataframes that also match the condition.

```python
mkdf = lambda x: pd.DataFrame(x)
data = [mkdf(i) for i in get_all_json("../resources/jsons") if mkdf(i)['data'].size < 8]
for df in data:
    print(df['name'])
```

    0    installers
    1    installers
    2    installers
    Name: name, dtype: object
    0    knote
    1    knote
    2    knote
    Name: name, dtype: object

Note that a generator by itself is just an object, so we must unpack it for it to work.

```python
data = get_all_json("../resources/jsons")
print(data)
```

    <generator object get_all_json at 0x114ce97d0>

## Footnotes

<sup><a id="fn.1" class="footnum" href="#fnr.1">1</a></sup> <https://en.wikipedia.org/wiki/Extract,_transform,_load>

<sup><a id="fn.2" class="footnum" href="#fnr.2">2</a></sup> <https://en.wikipedia.org/wiki/Regular_expression>

<sup><a id="fn.3" class="footnum" href="#fnr.3">3</a></sup> <https://en.wikipedia.org/wiki/Glob_(programming)>
